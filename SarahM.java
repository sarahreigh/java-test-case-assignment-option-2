package vut;

import java.util.Scanner;

/**
 *
 * @author 'Sarah Maluleke
 */
public class ConvertingNumbers
{

    static int numberToConvert;
    
    static Scanner input = new Scanner(System.in);

    public static void decimaToHexadecimal()
    {
        if ((numberToConvert >= 0) && (numberToConvert <= 10000))
        {
        System.out.println("Please enter the number you want to convert from Decimal to Hexadecimal:");
        numberToConvert = input.nextInt();
        System.out.println(Integer.toHexString(numberToConvert));

        }
        else
        {
            System.out.println("Please enter a number between 0 and 10000");
        }
    }

    public static void decimalToBinary()
    {
        if ((numberToConvert >= 0) && (numberToConvert <= 10000))
        {
             System.out.println("Please enter the number you want to convert from Decimal to Binary:");
            numberToConvert = input.nextInt();
            System.out.println(Integer.toBinaryString(numberToConvert));
           
        }
        else
        {
            System.out.println("Please enter a number between 0 and 10000");

        }
    }

    public static void binaryToDecimal()
    {
        String numberToConvert1=Integer.toString(numberToConvert);
        
        if ((numberToConvert >= 0) && (numberToConvert <= 10000))
        {
            System.out.println("Please enter the number you want to convert from Binary to Decimal:");
            numberToConvert = input.nextInt();
            System.out.println(Integer.parseInt(numberToConvert1,2));

        }
        else
        {
            System.out.println("Please enter a number between 0 and 10000");
        }
    }

    public static void main(String[] args)
    {

        decimaToHexadecimal();
        decimalToBinary();
        binaryToDecimal();

    }

}
